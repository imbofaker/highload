package handlers

import (
	"bitbucket.org/ofdastral/highload/db"
	"bitbucket.org/ofdastral/highload/filters"
	"github.com/gin-gonic/gin"
)

func Filter(c *gin.Context) {

	defer c.Request.Body.Close()

	filter := filters.InitFilter(c.Params)

	ft, err := db.Filter(filter)

	c.IndentedJSON(200, ft)
}
