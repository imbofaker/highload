package main

import (
	"bitbucket.org/ofdastral/highload/handlers"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	//получение сумм и количества чеков
	r.GET("/accounts/filter", handlers.Filter)
	r.Run()
}
