package parser

import (
	"encoding/json"
	"gopkg.in/mgo.v2"
	"log"
	"os"
)

type Account struct {
	Id        uint32   `json:"id"`
	Email     string   `json:"email"`     //100 символов юникод
	Fname     string   `json:"fname"`     //50 символов юникод
	Sname     string   `json:"sname"`     //50 символов юникод
	Phone     string   `json:"phone"`     //до 16 юникода
	Sex       string   `json:"sex"`       //m и f
	Birth     uint64   `json:"birth"`     //Ограничено снизу 01.01.1950, сверху 01.01.2005.
	Country   string   `json:"country"`   //50 символов юникод
	City      string   `json:"city"`      //50 символов юникод
	Joined    uint64   `json:"joined"`    //Ограничено снизу 01.01.2011, сверху 01.01.2018.
	Status    string   `json:"status"`    //«свободны», «заняты», «всё сложно» 1 2 3
	Interests []string `json:"interests"` //до 100 символов юникод
	Premium   Premium  `json:"premium"`
	Likes     []Like   `json:"likes"`
}

type Like struct {
	Id uint32 `json:"id"`
	Ts uint64 `json:"ts"`
}

type Premium struct {
	Start  float64 `json:"start"`  //Ограничено снизу 01.01.2018.
	Finish float64 `json:"finish"` //Ограничено снизу 01.01.2018.
}

type Records struct {
	Accounts []Account `json:"accounts"`
}

func main() {

	session, err := mgo.Dial("192.168.75.60:27017")
	if err != nil {
		log.Panic("conntection db error: ", err)
	}
	collection := session.DB("highload_db").C("accounts")

	var records Records

	Accs, err := os.Open("accounts_1.json")
	if err != nil {
		log.Println("opening accounts file", err.Error())
	}

	jsonParser := json.NewDecoder(Accs)
	if err = jsonParser.Decode(&records); err != nil {
		log.Println("parsing accounts file", err.Error())
	}

	for _, acc := range records.Accounts {

		err = collection.Insert(acc)
		if err != nil {
			log.Println("insert error: ", err)
		}
	}

	return
}
