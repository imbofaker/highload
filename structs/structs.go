package structs

type Account struct {
	Id        uint32   `json:"id,omitempty" bson:"id,omitempty"`
	Email     string   `json:"email,omitempty" bson:"email,omitempty"`         //100 символов юникод
	Fname     string   `json:"fname,omitempty" bson:"fname,omitempty"`         //50 символов юникод
	Sname     string   `json:"sname,omitempty" bson:"sname,omitempty"`         //50 символов юникод
	Phone     string   `json:"phone,omitempty" bson:"phone,omitempty"`         //до 16 юникода
	Sex       string   `json:"sex,omitempty" bson:"sex,omitempty"`             //m и f
	Birth     uint64   `json:"birth,omitempty" bson:"birth,omitempty"`         //Ограничено снизу 01.01.1950, сверху 01.01.2005.
	Country   string   `json:"country,omitempty" bson:"country,omitempty"`     //50 символов юникод
	City      string   `json:"city,omitempty" bson:"city,omitempty"`           //50 символов юникод
	Joined    uint64   `json:"joined,omitempty" bson:"joined,omitempty"`       //Ограничено снизу 01.01.2011, сверху 01.01.2018.
	Status    string   `json:"status,omitempty" bson:"status,omitempty"`       //«свободны», «заняты», «всё сложно» 1 2 3
	Interests []string `json:"interests,omitempty" bson:"interests,omitempty"` //до 100 символов юникод
	Premium   Premium  `json:"premium,omitempty" bson:"premium,omitempty"`
	Likes     []Like   `json:"likes,omitempty" bson:"likes,omitempty"`
}

type Like struct {
	Id uint32 `json:"id,omitempty" bson:"id,omitempty"`
	Ts uint64 `json:"ts,omitempty" bson:"ts,omitempty"`
}

type Premium struct {
	Start  float64 `json:"start,omitempty" bson:"start,omitempty"`   //Ограничено снизу 01.01.2018.
	Finish float64 `json:"finish,omitempty" bson:"finish,omitempty"` //Ограничено снизу 01.01.2018.
}
