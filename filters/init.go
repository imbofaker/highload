package filters

import (
	"bitbucket.org/ofdastral/highload/structs"
	"github.com/gin-gonic/gin"

	"log"
	"os"
)

var logger = log.New(os.Stdout, "", log.LstdFlags)

type Filter structs.Account

func InitFilter(params gin.Params) (filter Filter) {

	if val, ok := params.Get("sex"); ok {
		filter.Sex = val
	}

	return
}
